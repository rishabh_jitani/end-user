/**
 * Copyright (c) 2020 by Pulse Secure, LLC. All rights reserved.
 */

import React, { useState } from "react";
import "./PcsView.scss";
import ClientCard from "../ClientCard/ClientCard";
import {
  TextInput, Button, DialogOld, FlatButton, Select, PasswordInput
} from "psui-lib";
import BookmarksAccordion from "../BookmarksAccordion/BookmarksAccordion";
import ControlMenu from "../ControlMenu/ControlMenu";
import Atlassian from 'psui-lib/src/assets/images/illustrations/logos/services/atlassian.svg'
import Bitbucket from 'psui-lib/src/assets/images/illustrations/logos/services/bitbucket.svg'
import Dropbox from 'psui-lib/src/assets/images/illustrations/logos/services/dropbox.svg'
import Slack from 'psui-lib/src/assets/images/illustrations/logos/services/slack.svg'
import Confluence from 'psui-lib/src/assets/images/illustrations/logos/services/confluence.svg'
import Zulip from 'psui-lib/src/assets/images/illustrations/logos/services/zulip.svg'
import Zoom from 'psui-lib/src/assets/images/illustrations/logos/services/zoom.svg'
import { Switch, Route } from 'react-router-dom';
import ApplicationList from "../ApplicationList/ApplicationList";
const bookmarkList = [
  {
    serviceLogo: Bitbucket,
    title: "Bitbucket",
    count: 3,
    content:
      "Web resource profiles enable/disable access to web resources such as Intranet or Internet pages.",
    actionIcon: "icon-square-plus",
  },
  {
    serviceLogo: Dropbox,
    title: "Dropbox",
    count: 12,
    content:
      "File resource profiles enable/disable access to network file systems and directories.",
    actionIcon: "icon-square-plus",
  },
  {
    serviceLogo: Confluence,
    title: "Confluence",
    count: 8,
    content: "Specify applications to intermediate through W-SAM or J-SAM.",
    actionIcon: "icon-square-plus",
  },
  {
    serviceLogo: Slack,
    title: "Slack",
    count: 11,
    content:
      "Telnet/SSH resource profiles enable/disable access to terminal servers using Telnet or SSH protocols.",
    actionIcon: "icon-square-plus",
  },
  {
    serviceLogo: Atlassian,
    title: "Atlassian",
    count: 11,
    content:
      "Terminal Services resource profiles enable/disable access to Windows and Citrix terminal servers.",
    actionIcon: "icon-square-plus",
  },
  {
    serviceLogo: Zoom,
    title: "Zoom",
    count: 4,
    content:
      "Virtual Desktops resource profiles enable/disable access to VMware and Citrix Virtual Environments.",
    actionIcon: "icon-square-plus",
  },
  {
    serviceLogo: Zulip,
    title: "Zulip",
    count: 16,
    content:
      "HTML5 Access profiles enable/disable clientless access to terminal servers using RDP/SSH/Telnet protocols.",
    actionIcon: "icon-square-plus",
  }
];

const Dashboard = () => {
  const [visible, setVisibility] = useState(false);

  const handleOnClick = () => {
    setVisibility(!visible);
  }
  return (
    <div className="Page-wrapper">
      <div className="Page-wrapper-header">
        <div className="Page-wrapper-header__title">
          Welcome to the Pulse Connect Secure, Astokol.
        </div>
        <div className="Page-wrapper-header__sub_title">
          You last signed in on Wed, 29 July 2020 00:00:00 GMT from
          172.20.99.100
        </div>
      </div>
      <div className="Page-wrapper-content">
        <div className="pulseClientCard">
          <ClientCard />
        </div>
        <div className="search-container">
          <TextInput placeholder="Enter URL to browse..." />
          <div className="browse">Browse</div>
        </div>
        <Switch>
          <Route path="/applicationlist">
            <ApplicationList />
          </Route>
          <Route path="/">
            <div className="bookmarks-content">
              <BookmarksAccordion bookmarkList={bookmarkList} />
            </div>
          </Route>
        </Switch>
        <ControlMenu />
        <Button id="dialog-button" onClick={handleOnClick}>Click on me</Button>

        {/* Blocked Info Modal */}
        {/* <DialogOld
          title="Access Blocked by Administrator"
          iconClassName="icon-ban"
          isOpen={visible}
          className="warningModal blockedModal"
        >
          <DialogOld.Header></DialogOld.Header>
          <DialogOld.Content>
            <div className="modalContent">
              <h5 className="modalContent__subTitle">Access to this website is blocked by your administrator. Please notify your system administrator</h5>
              <p className="modalContent__details fontUppercase">Made https request for GET /?gws_rd=ssl HTTP/1.1 ti 10.209.113.62.443</p>
            </div>
          </DialogOld.Content>
          <DialogOld.Footer>
            <FlatButton type="secondary" padding onClick={handleOnClick}>Close</FlatButton>
            <FlatButton type="primary" padding onClick={handleOnClick}>Go to Dashboard</FlatButton>
          </DialogOld.Footer>
        </DialogOld> */}

        {/* Warning Info Modal */}
        {/* <DialogOld
          title="Warning"
          iconClassName="icon-alert"
          isOpen={visible}
          className="warningModal"
        >
          <DialogOld.Header></DialogOld.Header>
          <DialogOld.Content>
            <div className="modalContent">
              <h5 className="modalContent__subTitle">The page you are trying to access comes from a server with the following certificate problems.</h5>
              <p className="modalContent__infoLable"><strong>The certificate has Expired</strong></p>
              <p className="modalContent__details fontUppercase">You may choose to continue to the page. Your browsing will be secure, but the identity of the web server may be untrustworthy.</p>
            </div>
          </DialogOld.Content>
          <DialogOld.Footer>
            <FlatButton type="secondary" padding onClick={handleOnClick}>Close</FlatButton>
            <FlatButton type="primary" padding onClick={handleOnClick}>Continue to the page</FlatButton>
          </DialogOld.Footer>
        </DialogOld> */}

        {/* Authentication Modal */}
        {/* <DialogOld
          title="Authentication Required"
          iconClassName="icon-lock"
          isOpen={visible}
          className="warningModal"
        >
          <DialogOld.Header></DialogOld.Header>
          <DialogOld.Content>
            <div className="modalContent">
              <h5 className="modalContent__subTitle mb8">The webpage you are trying to access requires additional authentication.</h5>
              <p className="modalContent__details fontUppercase mb15">Please enter your username and password for this webpage.</p>
              <p className="modalContent__link"><a href="#">https://Parent-2K16.pcsqalab.net/</a></p>
              <div className="modalContent__form">
                <TextInput
                  label="Username"
                />
                <PasswordInput
                  label="Username"
                  type="password"
                />
                <Select
                  label="Choose option"
                  values={[
                    { 'label': 'This is interesting....', 'value': 'A' },
                    { 'label': 'This is interesting as well', 'value': 'B' }
                  ]}
                />
              </div>
            </div>
          </DialogOld.Content>
          <DialogOld.Footer>
            <FlatButton type="secondary" padding onClick={handleOnClick}>Close</FlatButton>
            <FlatButton type="primary" padding onClick={handleOnClick}>Continue to the page</FlatButton>
          </DialogOld.Footer>
        </DialogOld> */}

        {/* User sesstions Info Modal */}
        <DialogOld
          title="Warning"
          iconClassName="icon-alert"
          isOpen={visible}
          className="warningModal"
        >
          <DialogOld.Header></DialogOld.Header>
          <DialogOld.Content>
            <div className="modalContent">
              <h5 className="modalContent__subTitle mb20">There are already other user sesstions in progress.</h5>
              <div className="userDetails">
                <div className="userDetails__col ">
                  <p className="userDetails__title">Login IP Address</p>
                  <strong className="userDetails__info">172.20.99.165</strong>
                </div>
                <div className="userDetails__col">
                  <p className="userDetails__title">Last Access Time</p>
                  <strong className="userDetails__info">2020-07-31 02:42:17 +5:30 IST</strong>
                </div>
              </div>
              <p className="modalContent__details fontUppercase">Continue will result in termination of the other session.
Please select from one of the following options.</p>
            </div>
          </DialogOld.Content>
          <DialogOld.Footer>
            <FlatButton type="secondary" padding onClick={handleOnClick}>Close</FlatButton>
            <FlatButton type="primary" padding onClick={handleOnClick}>Continue to the page</FlatButton>
          </DialogOld.Footer>
        </DialogOld>
      </div>
    </div>
  );
};

export default Dashboard;
