/**
 * Copyright (c) 2020 by Pulse Secure, LLC. All rights reserved.
 */

import React from 'react';
import { withRouter } from 'react-router-dom';
import './BookmarkCard.scss';
import { Icon, IconButton } from 'psui-lib';
import PropTypes from 'prop-types';

const BookmarkCard = props => {
  const {serviceIcon = '', serviceLogo = ''} = props;
  return (
    <div className="Bookmark-card">
      {serviceIcon ? <Icon
        iconClassName={`Bookmark-card__serviceIcon ${serviceIcon}`}
        style={{ color: props.serviceIconColor }}
      />
      :
      <img
        src={serviceLogo}
        className='Bookmark-card__serviceLogo'
      />
      }
      <div className="Bookmark-card__content">
        <div className="Bookmark-card__content-title">
          {props.title}
          <span className="Bookmark-card__content-count">{props.count}</span>
        </div>
        <div className="Bookmark-card__content-para">{props.content}</div>
      </div>
      <IconButton
        iconClassName={`Bookmark-card__actionIcon ${props.actionIcon}`}
        style={{ color: props.actionIconColor }}
        onClick={() => { props.history.push('/resource'); }}
      />
    </div>
  );
};

BookmarkCard.propTypes = {
  serviceIcon: PropTypes.string,
  actionIcon: PropTypes.string,
  actionIconColor: PropTypes.string,
  serviceIconColor: PropTypes.string,
  title: PropTypes.string,
  content: PropTypes.string,
  count: PropTypes.number,
  history: PropTypes.func,
};

export default withRouter(BookmarkCard);