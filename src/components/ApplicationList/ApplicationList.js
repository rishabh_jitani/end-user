/**
 * Copyright (c) 2020 by Pulse Secure, LLC. All rights reserved.
 */

import React, { useState } from 'react';
import "./ApplicationList.scss";
import {
  Table, IconButton
} from "psui-lib";
import Bitbucket from 'psui-lib/src/assets/images/illustrations/logos/services/bitbucket.svg'

const ApplicationList = () => {


  const AppName = comp => {
    const row = comp.data[comp.row.index];
    console.log(row);
    if (row) {
      return (AppNameComp({ ...row, index: comp.row.index }));
    }
    return 'N/A';
  };

  const AppNameComp = (row) => {
    const { index } = row;
    return (<>
      <div className="applicationNameRow">
        <span className="icon"><img src={Bitbucket} /></span>
        Bitbucket
      </div>
    </>);
  };

  const Test = comp => {
    const row = comp.data[comp.row.index];
    console.log(row);
    if (row) {
      return (ActionComp({ ...row, index: comp.row.index }));
    }
    return 'N/A';
  };

  const ActionComp = (row) => {
    const { index } = row;
    return (<><IconButton
      iconClassName="icon-open2"
      id={`row-action${index}`}
    />
    </>);
  };

  return (
    <div className="applicationList">
      <Table
        data={[
          {
            applicationName: 'Bitbucket',
            type: 'Web Bookmarks',
            createdBy: 'System / Admin',
            details: 'Users, Finance',
          },
          {
            applicationName: 'Bitbucket',
            type: 'Web Bookmarks',
            createdBy: 'System / Admin',
            details: 'Users, Finance',
          },
        ]}
        columns={[
          { Header: 'Application Name', accessor: 'applicationName', Cell: (comp) => AppName(comp), sortable: true },
          { Header: 'Type', accessor: 'type', sortable: true },
          { Header: 'Created By', accessor: 'createdBy', sortable: true },
          { Header: 'Details', accessor: 'details' },
          { Header: '', accessor: 'action', Cell: (comp) => Test(comp), width: 50 },
        ]}
        height={300}
      >
      </Table>
    </div>
  );
};

export default ApplicationList;
