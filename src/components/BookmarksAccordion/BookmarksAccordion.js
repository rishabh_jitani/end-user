/**
 * Copyright (c) 2020 by Pulse Secure, LLC. All rights reserved.
 */

import React, { useEffect, useState } from 'react';
import './BookmarksAccordion.scss';
import { AccordionMenu, AccordionSubMenu } from 'psui-lib';
import BookmarkCard from '../BookmarkCard/BookmarkCard';

const BookmarksAccordion = props => {

  const [showAccordian, accordianChange] = useState(true);

  const onAccordianChange = () => {
    accordianChange(!showAccordian)
  }




  return (
    <div>
      <AccordionMenu>
        <AccordionSubMenu
          index="1"
          isOpen={showAccordian}
          onClick={onAccordianChange}
          label={
            <span>Web Bookmarks</span>
          }
        >
          <div className="bookmark-card-list">
            {props.bookmarkList.map((bookmark, i) => {
              return <BookmarkCard key={i} {...bookmark} />;
            })}
          </div>
        </AccordionSubMenu>
      </AccordionMenu>


    </div>
  )
}

export default BookmarksAccordion;