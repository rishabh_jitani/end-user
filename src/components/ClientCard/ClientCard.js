import React from "react";
import "./ClientCard.scss";
import { Button, Logo } from "psui-lib";

export default function ClientCard({ title, onClick }) {
  return (
    <div className="card">
      <div className="container">
        <Logo color="black-green" variant="no-text" />
        <div className="text">Pulse Unified Client</div>
        <Button color="green">Start</Button>
      </div>
    </div>
  );
}
