/**
 * Copyright (c) 2020 by Pulse Secure, LLC. All rights reserved.
 */

import React from 'react';
import { withRouter } from 'react-router-dom';
import './ControlMenu.scss';
import { Icon, FloatingButton, Logo } from 'psui-lib';

const ControlMenu = props => {
  return (
    <div className="controlMenu">
      <div className="controlMenuRow">
        <div className="controlDragAction">
          <FloatingButton
            iconClassName="icon-ellipsis2"
          />
        </div>
        <div className="controlMenuActoin">
          <div className="MenuActoinItem">
            <Logo color="black-green" variant="no-text" />
          </div>
          <div className="MenuActoinItem">
            <FloatingButton
              iconClassName="icon-bookmark"
            />
          </div>
          <div className="MenuActoinItem">
            <FloatingButton
              iconClassName="icon-info"
            />
          </div>
          <div className="MenuActoinItem">
            <FloatingButton
              iconClassName="icon-exit"
            />
          </div>
        </div>
        <div className="controlExpandAction">
          <FloatingButton
            iconClassName="icon-chevron-left"
          />
        </div>
      </div>
    </div>
  )
}

export default ControlMenu;