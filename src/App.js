import React from 'react';
import logo from './logo.svg';
import './App.css';
import PcsView from './components/PcsView'

function App() {
  return (
    <div className="App">
      <PcsView />
    </div>
  );
}

export default App;
